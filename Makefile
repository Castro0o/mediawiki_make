
include .env
MAKE_DIR := $(shell pwd)
BUILD_DIR := $(BUILD_DIR)
MW_DIR_ABS = $(BUILD_DIR)$(MW_DIR)
mw_release_url = https://releases.wikimedia.org/mediawiki/1.34/mediawiki-1.34.1.tar.gz
mw_tarball = $(shell basename $(mw_release_url))
mw_dir_default = $(shell basename $(mw_release_url) .tar.gz)
sql_script = script.sql
composer_file = composer.local.json
mw_path_nohttp = $(shell basename $(MW_SERVER)) # for smw setting

.ONESHELL:
test:
# 	$(eval EXTDIR = "./extensions/PageForms")
	echo $(MAKE_DIR)

# 	cd $(MW_DIR_ABS)
# 	echo $(REGULAR_USER) $(MW_DIR_ABS)
# 	runuser -l $(REGULAR_USER) -c 'touch foo'
# 	su $(REGULAR_USER)
# 	echo $(MW_DIR_ABS)
# 	pwd
# 	ls .
# 	whoami 
# 	exit

dependencies:
	# base in https://www.mediawiki.org/wiki/Manual:Installation_requirements
	apt install -y mariadb-server
	mysql_secure_installation
	apt install -y apache2
	apt install -y  php libapache2-mod-php php-apcu php-intl php-mbstring php-xml php-mysql
	systemctl restart apache2
	apt install -y imagemagick

untar:
	@echo $(mw_tarball)	
	@echo $(mw_dir_default)
	
	if [ ! -f $(mw_tarball) ]; then curl $(mw_release_url) --output $(mw_tarball); fi
	tar xfvz $(mw_tarball)
	if [ ! -d $(MW_DIR_ABS) ]; then mv $(mw_dir_default) $(MW_DIR_ABS); fi

db:
	# create db via $(sql_script)
	@echo "CREATE DATABASE IF NOT EXISTS $(MYSQL_DATABASE);" > $(sql_script)
	@echo "CREATE USER IF NOT EXISTS '$(MYSQL_USER)'@$(MYSQL_HOST) IDENTIFIED BY '$(MYSQL_PASSWORD)';" >> $(sql_script)
	@echo "GRANT ALL PRIVILEGES ON $(MYSQL_DATABASE).* TO '$(MYSQL_USER)'@$(MYSQL_HOST);" >> $(sql_script)
	@echo "FLUSH PRIVILEGES;" >> $(sql_script) 
	mariadb -h $(MYSQL_HOST) < $(sql_script)
	rm $(sql_script)
	# create user

.ONESHELL:
mw_install: untar db
	cd $(MW_DIR_ABS)
	php maintenance/install.php \
--dbname=$(MYSQL_DATABASE) \
--dbpass=$(MYSQL_PASSWORD) \
--dbserver=$(MYSQL_HOST) \
--dbtype=mysql \
--dbuser=$(MYSQL_USER) \
--installdbpass=$(MYSQL_PASSWORD) \
--installdbuser=$(MYSQL_USER) \
--lang=$(MW_WIKILANG) \
--scriptpath=/$(MW_DIR) \
--server=$(MW_SERVER) \
--pass $(MW_PASSWORD) \
$(MW_WIKINAME) $(MW_WIKIUSER)
	cd $(MAKE_DIR)
	cat error_settings.php  >> $(MW_DIR_ABS)/LocalSettings.php
	chown www-data:www-data $(MW_DIR_ABS) -R

.ONESHELL:
mw_extensions_composer:
	# extensions installed via composer
	# uses: extensions_composer.yml & composer.local.json
	# TODO: CREATE composer.local.json FROM extensions_composer.yml
	echo "############">> $(MW_DIR_ABS)/LocalSettings.php; 
	for i in `cat extensions_composer.yml | yq -c ".[][]"`; \
	do \
	str=`echo $$i|jq ".[].str"|xargs`; \
	settings=`echo $$i|jq ".[].settings"|xargs`; \
	if [ "$$str" = "SMW" ]; then \
		echo "enableSemantics( '$(mw_path_nohttp)' );" >> $(MW_DIR_ABS)/LocalSettings.php; \
	else \
		echo $$str >> $(MW_DIR_ABS)/LocalSettings.php; \
	fi; \
	if [ "$$settings" != "null" ]; then \
	 echo $$settings >> $(MW_DIR_ABS)/LocalSettings.php; \
	fi; \
	done
	cp $(composer_file) $(MW_DIR_ABS)
	cd $(MW_DIR_ABS)
	touch composer.lock
	chmod a+rw vendor/ extensions/ composer.lock -R #  dirs should be writable by REGULAR_USER
	curl -sS https://getcomposer.org/installer | php
	runuser -l $(REGULAR_USER) -c 'cd $(MW_DIR_ABS); php composer.phar update --no-dev'
	php maintenance/update.php


.ONESHELL:
mw_extensions_default:
	# extensions already bundled with mediawiki installer
	# uses: extensions.yml
	for i in `cat extensions.yml | yq -c ".[][]"`; \
	do \
	str=`echo $$i|jq ".[].str"|xargs`; \
	settings=`echo $$i|jq ".[].settings"|xargs`; \
	echo $$str >> $(MW_DIR_ABS)/LocalSettings.php; \
	if [ "$$settings" != "null" ]; then \
	 echo $$settings >> $(MW_DIR_ABS)/LocalSettings.php; \
	fi \
	done

.ONESHELL:
mw_extensions_git:
	# extensions installed via git
	# uses: extensions_git.yml 
	for i in `cat extensions_git.yml | yq -c ".[][]"`; \
	do \
	dir=$(MW_DIR_ABS)/`echo $$i|jq ".[].dir"|xargs`; \
	repo=`echo $$i|jq ".[].repo"|xargs`; \
	str=`echo $$i|jq ".[].str"|xargs`; \
	settings=`echo $$i|jq ".[].settings"|xargs`; \
	git clone $$repo $$dir; \
	echo $$str >> $(MW_DIR_ABS)/LocalSettings.php; \
	if [ "$$settings" != "null" ]; then \
	 echo $$settings >> $(MW_DIR_ABS)/LocalSettings.php; \
	fi \
	done



mw_extensions: mw_extensions_composer mw_extensions_default mw_extensions_git 



install: untar db mw_install mw_extensions
	chown www-data:www-data $(MW_DIR_ABS) -R



dropdb:
	@echo "DROP USER IF EXISTS '$(MYSQL_USER)'@$(MYSQL_HOST);" > $(sql_script)
	@echo "DROP DATABASE IF EXISTS $(MYSQL_DATABASE);" >> $(sql_script)
	mariadb -h $(MYSQL_HOST) < $(sql_script)
	rm $(sql_script)

mw_uninstall: dropdb
	if [ -d $(MW_DIR_ABS) ]; then rm -r $(MW_DIR_ABS); fi

clean:
	if [ -f $(mw_tarball)]; then rm -r $(mw_tarball); fi
