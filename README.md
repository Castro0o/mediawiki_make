
# Variables in .env file 
* `BUILD_DIR` specifies the web directory, where the mw_dir will be located. **Needs to end with /**
* `MW_DIR` the name of the wiki directory - will also define its path
* `REGULAR_USER` the system regular user (not root) - required to run php composer.

# Extensions:
Defines in file extensions.yaml, extensions_git.yaml, extensions_composer.yaml, 
* composer: also need editing in composer.local.json

Follow the same scheme and ensure **value** value contains no empty spaces;

```yaml
- PageForms:
  - dir: extensions/PageForms
    repo: https://gerrit.wikimedia.org/r/mediawiki/extensions/PageForms.git
    str: wfLoadExtension('PageForms');
    settings: $wgGroupPermissions['*']['viewedittab']=false;\n$wgGroupPermissions['sysop']['viewedittab']=true;
```
* **settings** is optional. 
	* If used keep all settings with one line


# Commands

`sudo make install`
* downloads mw tarball: `sudo make untar`
	* untars and mv the dir
* creates db and db user: `sudo make db`
* installs mediawiki: `sudo make mw_install`
* installs extensions: `sudo make mw_extensions`

`sudo make mw_uninstall`
* drops db & user: `sudo make dropdb`
* rm mw directory


# TODO 
* environmental checks on MW dependencies
   * if they are not install: call rule to install them


# Dependencies
* yq `pip3 install yq --user`
* jq
* git

for row in $(cat extensions.yml | yq -c "."| base64); do echo ${row}; decode=$(echo ${row}|base64 --decode); echo ${decode}; done